import os
import sys
sys.path
sys.path.append('/home/julius/Projects/phd/tgff_multi_phase/')
# from scripts import solve_all_parallel, extract_scheduling_results
from scripts import solve_all_parallel

solve_all_parallel.computed_DAGS(os.getcwd(), os.getcwd(), utilization=True)
# extract_scheduling_results.generate_df_from_solve('/home/julius/Projects/phd/tgff_experiments/output/',  files = {0: 'ILP.txt', 1: 'heuristic.txt', 2: 'makespan.txt', 3: 'makespan_single.txt'}, second_dir="/home/julius/Projects/phd/tgff_experiments/rodinia_cross_over_small")
