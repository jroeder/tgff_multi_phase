#!/bin/bash
#SBATCH -t 02:05:00 -N 1 --cores-per-socket=16 --mem=90G
#SBATCH --error=/home/jroeder/Projects/phd/tgff_multi_phase/3-phase-data-smaller-many/hFLS_output_100_6ms.txt
#SBATCH --output=/home/jroeder/Projects/phd/tgff_multi_phase/3-phase-data-smaller-many/hFLS_output_100_6ms.txt

module load 2019
module load GCC/8.3.0
module load GCCcore/8.3.0
module load Boost/1.67.0-intel-2019b
module load libxml++/2.40.1-GCCcore-8.3.0
module unload XZ/5.2.4-GCCcore-8.3.0

mkdir "$TMPDIR"/DAG2030_output_dir

$HOME/Projects/phd/TeamPlay_Tools/CoordinationTools/compiler/build/dist/methane --config  $HOME/Projects/phd/tgff_multi_phase/hFLS.xml --coord $HOME/Projects/phd/tgff_multi_phase/3-phase-data-smaller-many/DAG2030/DAG2030.tey --nfp $HOME/Projects/phd/tgff_multi_phase/empty.nfp --outdir "$TMPDIR"/DAG2030_output_dir

cp -r "$TMPDIR"/DAG2030_output_dir $HOME/Projects/phd/tgff_multi_phase/3-phase-data-smaller-many/TRASH/
