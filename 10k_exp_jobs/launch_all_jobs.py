import os
import subprocess

path = "/home/jroeder/Projects/phd/tgff_multi_phase/10k_exp_jobs/jobs/"
out_path = "/home/jroeder/Projects/phd/tgff_multi_phase/3-phase-data-smaller-many/"

folders_out = os.listdir(out_path)

print(folders_out)

files = os.listdir(path)

for folder_out in folders_out:

    if "DAG" not in folder_out:
        continue

    files_in_folder = os.listdir(os.path.join(out_path, folder_out))
    for file in files_in_folder:
        if "hFLS_output_100_6ms.txt" not in file:
            continue

        for i,f in enumerate(files):
            if (folder_out + ".sh") == f:
                print(f)
                del files[i]

            if ".sh" not in f:
                del files[i]

print(files)
print(len(files))

for f in files[:1]:
    f_path = os.path.join(path,f)
    print(f_path)
    subprocess.call(["sbatch",f_path])
    # break
