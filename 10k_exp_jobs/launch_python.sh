#!/bin/bash
#SBATCH -t 00:05:00 -N 1 --cores-per-socket=16 --mem=90G
#SBATCH --error=/home/jroeder/Projects/phd/tgff_multi_phase/3-phase-data-smaller-many/python_logs.txt
#SBATCH --output=/home/jroeder/Projects/phd/tgff_multi_phase/3-phase-data-smaller-many/python_logs.txt

module load 2019
module load GCC/8.3.0
module load GCCcore/8.3.0
module load Boost/1.67.0-intel-2019b
module load libxml++/2.40.1-GCCcore-8.3.0
module load GDAL/2.2.3-intel-2019b-Python-3.6.6
module unload XZ/5.2.4-GCCcore-8.3.0


python analysis_epyc.py
#$HOME/Projects/phd/TeamPlay_Tools/CoordinationTools/compiler/build/dist/methane --config  $HOME/Projects/phd/tgff_multi_phase/hFLS.xml --coord $HOME/Projects/phd/tgff_multi_phase/3-phase-data-smaller-many/REPLACE/REPLACE.tey --nfp $HOME/Projects/phd/tgff_multi_phase/empty.nfp --outdir "$TMPDIR"/REPLACE_output_dir

