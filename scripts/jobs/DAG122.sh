#!/bin/bash
#SBATCH -t 12:05:00 -N 1 --cores-per-socket=16 --mem=90G
#SBATCH --error=/home/jroeder/Projects/phd/tgff_multi_phase/3-phase-data-ILP-out/DAG122_slurm-%j.out
#SBATCH --output=/home/jroeder/Projects/phd/tgff_multi_phase/3-phase-data-ILP-out/DAG122_slurm-%j.out

mkdir $HOME/Projects/phd/tgff_multi_phase/3-phase-data-ILP-out/DAG122/

module load 2019
module load GCC/8.3.0
module load GCCcore/8.3.0
module load Boost/1.67.0-intel-2019b
module load libxml++/2.40.1-GCCcore-8.3.0
module unload XZ/5.2.4-GCCcore-8.3.0

mkdir "$TMPDIR"/DAG122_output_dir

$HOME/Projects/phd/TeamPlay_Tools/CoordinationTools/compiler/build/dist/methane --config  $HOME/Projects/phd/tgff_multi_phase/ilp.xml --coord $HOME/Projects/phd/tgff_multi_phase/3-phase-data-ILP/DAG122/DAG122.tey --nfp $HOME/Projects/phd/tgff_multi_phase/empty.nfp --outdir "$TMPDIR"/DAG122_output_dir

cp -r "$TMPDIR"/DAG122_output_dir $HOME/Projects/phd/tgff_multi_phase/3-phase-data-ILP-out/TRASH/
