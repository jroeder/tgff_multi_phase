import os
from multiprocessing import Pool

def get_num(in_str: str):
    '''
    :param in_str:
    :return: just returns a single consecutive digit
    '''
    num = ""

    for s in in_str:
        if s.isdigit():
            num += s

    return int(num)


def compute_utilization(input_list: list):

    taskgraph, target_files = input_list

    makespan = 0
    period = 0

    for file in target_files:
        file_path = os.path.join(taskgraph, file)

        with open(file_path, 'r') as f:
            for line in f:
                split_l = line.split(' ')
                if 'Makespan' in line:
                    makespan = int(split_l[1])
                    break
                elif 'period' in line:
                    period = get_num(line)
                    break

    assert period > 0, "Period needs to be larger than 0."
    assert makespan > 0, "Makespan needs to be larger than 0. " + taskgraph
    utilization = makespan/period

    out_path = os.path.join(taskgraph, "utilization.txt")

    with open(out_path, 'w') as out_file:
        out_file.write("Utilization: " + str(utilization))

def computed_DAGS(directory: str):
    list_dags = os.listdir(directory)

    input = []

    for dag in list_dags:
        if "DAG" in dag:
            temp = [os.path.join(directory, dag), [dag+".tey", "sequential_schedule.txt"]]
            input.append(temp)

    with Pool(16) as pool:
        pool.map(compute_utilization, input)


if __name__ == "__main__":
    # computed_DAGS('../utilization/3-phase-data/')
    # computed_DAGS('../utilization/3-phase-data-ILP/')
    computed_DAGS('../utilization/3-phase-data-smaller/')