import os
import subprocess

path = "/home/julius/Projects/phd/tgff_multi_phase/scripts/jobs/"
out_path = "/home/julius/Projects/phd/tgff_multi_phase/3-phase-data-ILP-out/"

folders_out = os.listdir(out_path)

print(folders_out)

files = os.listdir(path)

for folder_out in folders_out:
    if ".out" not in folder_out:
        continue
    begin_slurm = folder_out.find("_")
    out_name = folder_out[:begin_slurm]
    if begin_slurm == -1:
        out_name = folder_out[:-4]


    for i,f in enumerate(files):
        if (out_name + ".sh") == f:
            print(f)
            del files[i]

        if ".sh" not in f:
            del files[i]

print(files)
print(len(files))

for f in files[:50]:
    f_path = os.path.join(path,f)
    print(f_path)
    # subprocess.call(["sbatch",f_path])
    # break
