import os
import subprocess

def computed_DAGS(directory):
    list_dags = os.listdir(directory)

    for i, dag in enumerate(list_dags):
        dag_path = os.path.join(directory,dag)
        dag_command = []
        if os.path.isdir(dag_path) and "DAG" in dag_path:
            dag_command = ""
            with open("base_script.sh", "r") as target:
                for l in target:
                    if "REPLACE" in l:
                        dag_command += l.replace("REPLACE", dag)
                    else:
                        dag_command  += l
            with open("jobs/"+dag+".sh", "w") as target:
                for l in dag_command:
                    target.write(l)

if __name__ == '__main__':
    computed_DAGS("/home/julius/Projects/phd/tgff_multi_phase/3-phase-data-ILP/")
