import os
import subprocess
from multiprocessing import Pool, Manager
import ctypes

def compute_DAG(data):
    list_dags, directory, output_dir, ilp, heuristic, makespan, heuristic_single, utilization, num, counter_lock = data
    list_dags = [list_dags]

    ilp_command = ["/home/jroeder/Projects/phd/TeamPlay_Tools/CoordinationTools/compiler/build/dist/methane", "--config", "../ilp.xml", "--coord", "REPLACE/REPLACE.tey", "--nfp", "../empty.nfp"]
    makespan_command = ["/home/jroeder/Projects/phd/TeamPlay_Tools/CoordinationTools/compiler/build/dist/methane", "--config", "REPLACE/configs/makespan/makespan.xml", "--coord", "REPLACE/REPLACE.coord", "--nfp", "REPLACE/non_func_prop.nfp"]
    heuristic_single_command = ["/home/jroeder/Projects/phd/TeamPlay_Tools/CoordinationTools/compiler/build/dist/methane", "--config", "../singleFLS.xml", "--coord", "REPLACE/REPLACE.tey", "--nfp", "../empty.nfp"]
    heuristic_command = ["/home/jroeder/Projects/phd/TeamPlay_Tools/CoordinationTools/compiler/build/dist/methane", "--config", "../hFLS.xml", "--coord", "REPLACE/REPLACE.tey", "--nfp", "../empty.nfp"]
    utilization_command = ["/home/jroeder/Projects/phd/TeamPlay_Tools/CoordinationTools/compiler/build/dist/methane", "--config", "../../Utilization.xml", "--coord", "REPLACE/REPLACE.tey", "--nfp", "../../empty.nfp"]

    for i, dag in enumerate(list_dags):
        dag_command = []
        if os.path.isdir(dag) and "DAG" in dag:
            original = os.path.join(directory, dag, os.path.basename(dag) + ".svg")

            if ilp:
                for part in ilp_command:
                    dag_command.append(part.replace("REPLACE", os.path.basename(dag)))
                out = os.path.join (output_dir, dag + ".out")
                if not os.path.exists(os.path.join (output_dir,dag)):
                    os.makedirs(os.path.join (output_dir,dag))
                if not os.path.exists(out):
                    ilp_desc = open(out, 'w')
                    subprocess.call(dag_command, stdout=ilp_desc, stderr=subprocess.STDOUT)
                    ilp_desc.close()
                    new = os.path.join(output_dir, dag, os.path.basename(dag) + "_ilp.svg")
                    if not os.path.exists(os.path.join(output_dir, dag)):
                        os.makedirs(os.path.join(output_dir, dag))
                    try:
                        os.rename(original, new)
                    except:
                        pass

            if makespan:
                dag_command = []
                for part in makespan_command:
                    dag_command.append(part.replace("REPLACE", dag))
                out = os.path.join (output_dir,dag,"makespan.txt")
                if not os.path.exists(os.path.join (output_dir,dag)):
                    os.makedirs(os.path.join (output_dir,dag))
                if not os.path.exists(out):
                    ilp_desc = open(out, 'w')
                    subprocess.call(dag_command, stdout=ilp_desc, stderr=subprocess.STDOUT)
                    ilp_desc.close()
                    new = os.path.join(output_dir, dag, os.path.basename(dag) + "_makespan.svg")
                    if not os.path.exists(os.path.join(output_dir, dag)):
                        os.makedirs(os.path.join(output_dir, dag))
                    try:
                        os.rename(original, new)
                    except:
                        pass

            if heuristic_single:
                dag_command = []
                for part in heuristic_single_command:
                    dag_command.append(part.replace("REPLACE", dag))
                out = os.path.join (output_dir,dag,"SINGLE_PHASE.txt")
                if not os.path.exists(os.path.join (output_dir,dag)):
                    os.makedirs(os.path.join (output_dir,dag))
                # if not os.path.exists(out):
                ilp_desc = open(out, 'w')
                subprocess.call(dag_command, stdout=ilp_desc, stderr=subprocess.STDOUT)
                ilp_desc.close()
                new = os.path.join(output_dir, dag, os.path.basename(dag) + "_cpu.svg")
                if not os.path.exists(os.path.join(output_dir, dag)):
                    os.makedirs(os.path.join(output_dir, dag))
                try:
                    os.rename(original, new)
                except:
                    pass

            if heuristic:
                dag_command = []
                for part in heuristic_command:
                    dag_command.append(part.replace("REPLACE", dag))
                out = os.path.join (output_dir,dag,"hFLS_output_100_6ms.txt")
                if not os.path.exists(os.path.join (output_dir,dag)):
                    os.makedirs(os.path.join (output_dir,dag))
                if not os.path.exists(out):
                    ilp_desc = open(out, 'w')
                    subprocess.call(dag_command, stdout=ilp_desc, stderr=subprocess.STDOUT)
                    ilp_desc.close()
                    new = os.path.join(output_dir, dag, os.path.basename(dag) + "_hFLS.svg")
                    try:
                        os.rename(original, new)
                    except:
                        pass

            if utilization:
                dag_command = []
                for part in utilization_command:
                    dag_command.append(part.replace("REPLACE", dag))
                out = os.path.join(output_dir, dag, "sequential_schedule.txt")
                if not os.path.exists(os.path.join(output_dir, dag)):
                    os.makedirs(os.path.join(output_dir, dag))
                # if not os.path.exists(out):
                ilp_desc = open(out, 'w')
                subprocess.call(dag_command, stdout=ilp_desc, stderr=subprocess.STDOUT)
                ilp_desc.close()
                new = os.path.join(output_dir, dag, os.path.basename(dag) + "_utilization.svg")
                try:
                    os.rename(original, new)
                except:
                    pass

            with counter_lock:
                num.value += 1
                print("DAG " + os.path.basename(dag) + " done. Number: " + str(num.value))


def computed_DAGS(directory, output_dir, ilp=False, heuristic=False, makespan=False, heuristic_single=False, utilization=False):
    list_dags = sorted(os.listdir(directory))

    #list_dags = list_dags[9000:10000]

    input = []

    manager = Manager()
    num = manager.Value(ctypes.c_ulonglong, 0)
    counter_lock = manager.Lock()

    for dag in list_dags:
        temp = []
        temp.append(dag)
        temp.append(directory)
        temp.append(output_dir)
        temp.append(ilp)
        temp.append(heuristic)
        temp.append(makespan)
        temp.append(heuristic_single)
        temp.append(utilization)
        temp.append(num)
        temp.append(counter_lock)

        input.append(temp)


    with Pool(8) as pool:
        pool.map(compute_DAG, input)

